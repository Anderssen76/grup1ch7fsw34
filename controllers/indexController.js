const home = require('./homeController');
const work = require('./workController');
const contact = require('./contactController');
const about = require('./aboutController');
const signup = require('./signupController');
const login = require('./loginController');
const dashboard = require('./dashboardController');
const userEdit = require('./userEditController');

module.exports ={
	home,
    work,
    contact,
    about,
	signup,
	login,
	dashboard,
    userEdit
};
