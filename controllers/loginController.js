const {sequelize,
  User_Game,
  User_Game_Biodata,
  User_Game_History,
} = require("../models");

const passport = require('../lib/passport' );

function format(user) {
  const { id, username } = user
  return {
  id,
  username,
  accessToken : user.generateToken ()
  }
  }


const login = (req, res) => {
    res.render("login");
  };

  const authLogin = (req, res) => {
    User_Game.authenticate(req.body)
    .then (user => {
      res.json(format(user))
    })}


  
  // const authLogin = passport.authenticate('local', {
  //   successRedirect: '/user',
  //   failureRedirect: '/login',
  //   failureFlash: true // Untuk mengaktifkan express flash
  // });




  const whoami = (req, res) => {
    /* req.user adalah instance dari User Model, hasil autentikasi dari passport. */
    // res.render('user', req.user.dataValues)
    const currentUser = req.user;
res.json(currentUser)
    }
    



// // authentication & authorization login method
// const authLogin = async (req, res) => {
//     const { email, password } = req.body;
  
//     const user = User_Game.find(
//       (user) => user.email === email && user.password === password
//     );
  
//     const superuser = User_Game.find(
//       (superuser) =>
//         superuser.email === email &&
//         superuser.password === password &&
//         superuser.role === "admin"
//     );
  
//     if (!user) {
//       res.status(401).render("login", { message: "Wrong Email or Password!" });
//     } else if (superuser) {
//       try {
//         const user_game = await User_Game.findAll();
//         const user_game_biodata = await User_Game_Biodata.findAll();
//         const user_game_history = await User_Game_History.findAll();
//         res.render("dashboard", {
//           user_game,
//           user_game_biodata,
//           user_game_history,
//         });
//       } catch (err) {
//         console.error("Error retrieving data:", err);
//         res.status(500).send("Internal Server Error");
//       }
//     } else {
//       const { username } = user;
//       res.render("greet", { username });
//     }
//   };

  
  // exports.login = login;
  // exports.authLogin = authLogin;
  // exports.whoami = whoami;

  module.exports = { login, authLogin, whoami };
  