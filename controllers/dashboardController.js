const {
    User_Game,
    User_Game_Biodata,
    User_Game_History,
  } = require("../models");


  const userData = async (req, res) => {
    try {
      const user_game = await User_Game.findAll();
      const user_game_biodata = await User_Game_Biodata.findAll();
      const user_game_history = await User_Game_History.findAll();
      res.render("dashboard", {
        user_game,
        user_game_biodata,
        user_game_history,
      });
    } catch (err) {
      console.error("Error retrieving data:", err);
      res.status(500).send("Internal Server Error");
    }
  };


// create new user
  const userCreate = async (req, res) => {
    try {
      const userGame = await User_Game.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role,
      });
      await User_Game_Biodata.create({
        first_name: req.body.firstname,
        last_name: req.body.lastname,
        gender: req.body.gender,
        country: req.body.country,
        user_id: userGame.id,
        date_of_birth: req.body.birthday,
      });
      const user_game = await User_Game.findAll();
      const user_game_biodata = await User_Game_Biodata.findAll();
      const user_game_history = await User_Game_History.findAll();
      res.render("dashboard", {
        user_game,
        user_game_biodata,
        user_game_history,
      });
    } catch (err) {
      console.log(err);
      res.status(500).send("Terjadi kesalahan dalam membuat pengguna");
    }
  };

  module.exports = { userData, userCreate};
