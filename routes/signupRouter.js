const router = require('express' ).Router()
// Controllers
const signup = require('../controllers/indexController').signup

// Register Page
router.get('/signup' , signup.getSignup );
router.post('/signup' , signup.authSignup );


module.exports = router;