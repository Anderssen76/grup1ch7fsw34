const router = require("express").Router();
const loginController = require("../controllers/indexController").login;
const restrict = require('../middlewares/restrict').restrict;

router.get("/login", loginController.login);

// CREATE
router.post("/login", loginController.authLogin);

router.get('/', restrict, (req, res) => res.render('index'));

router.get('/user', restrict, loginController.whoami);

// router.post('/login', passport.authenticate('local', {
//   successRedirect: '/',
//   failureRedirect: '/login',
//   failureFlash: true
//   }))
  


  module.exports = router;