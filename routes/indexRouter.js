const router = require("express").Router();
const homeController = require("../controllers/indexController").home;
const workController = require("../controllers/indexController").work;
const aboutController = require("../controllers/indexController").about;
const contactController = require("../controllers/indexController").contact;

router.get("/", homeController.home);
router.get("/aboutme", aboutController.about);
router.get("/work", workController.work);
router.get("/contact", contactController.contact);
  
  module.exports = router;