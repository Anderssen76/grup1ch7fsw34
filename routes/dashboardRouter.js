const router = require("express").Router();
const dashboardController = require("../controllers/indexController").dashboard;

router.get("/dashboard", dashboardController.userData);

// CREATE
// router.post("/login", loginController.authLogin);

  module.exports = router;