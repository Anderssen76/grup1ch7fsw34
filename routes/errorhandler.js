const router = require("express").Router();

// Internal server error
router.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).json({
    status: "failed to connect",
    errors: err.message,
  });
});

// 404 error
router.use(function (req, res, next) {
  res.status(404).send("<h1>404 : Page Not Found</h1>");
});

module.exports = router;
