const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const morgan = require('morgan');
const path = require("path");
const { port = 3000 } = process.env;
const {
  sequelize,
  User_Game,
  User_Game_Biodata,
  User_Game_History,
} = require("./models");
const passport = require("./lib/passport")
const flash = require('express-flash')
const session = require('express-session')
// let users = require("./db/users.json");
// const flash = require('express-flash')
app.use(session({
  secret: "make it secret",
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// routes
const userProfile = require("./routes/userprofile");
const errorHandler = require("./routes/errorhandler");
const indexRoutes = require("./routes/indexRouter");
const loginRoutes = require("./routes/loginRouter");
const signupRoutes = require("./routes/signupRouter");
const dashboardRoutes = require("./routes/dashboardRouter");

// can't login message on login.ejs
app.locals.message = null;

app.set("view engine", "ejs");

app.use(morgan('combined'));

app.use(express.static(path.join(__dirname, "public")));

app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());



// routes for static page (home, work, aboutme, contact)
app.use(indexRoutes);
app.use(loginRoutes);
app.use(signupRoutes);
app.use(dashboardRoutes);

// // static login method
// app.post("/users", async (req, res) => {
//   const { email, password } = req.body;

//   const user = users.find(
//     (user) => user.email === email && user.password === password
//   );

//   const superuser = users.find(
//     (superuser) =>
//       superuser.email === email &&
//       superuser.password === password &&
//       superuser.role === "admin"
//   );

//   if (!user) {
//     res.status(401).render("login", { message: "Wrong Email or Password!" });
//   } else if (superuser) {
//     try {
//       const user_game = await User_Game.findAll();
//       const user_game_biodata = await User_Game_Biodata.findAll();
//       const user_game_history = await User_Game_History.findAll();
//       res.render("dashboard", {
//         user_game,
//         user_game_biodata,
//         user_game_history,
//       });
//     } catch (err) {
//       console.error("Error retrieving data:", err);
//       res.status(500).send("Internal Server Error");
//     }
//   } else {
//     const { username } = user;
//     res.render("greet", { username });
//   }
// });

// create new user
// app.post("/users/create", async (req, res) => {
//   try {
//     const userGame = await User_Game.create({
//       username: req.body.username,
//       email: req.body.email,
//       password: req.body.password,
//       role: req.body.role,
//     });
//     await User_Game_Biodata.create({
//       first_name: req.body.firstname,
//       last_name: req.body.lastname,
//       gender: req.body.gender,
//       country: req.body.country,
//       user_id: userGame.id,
//       date_of_birth: req.body.birthday,
//     });
//     const user_game = await User_Game.findAll();
//     const user_game_biodata = await User_Game_Biodata.findAll();
//     const user_game_history = await User_Game_History.findAll();
//     res.render("dashboard", {
//       user_game,
//       user_game_biodata,
//       user_game_history,
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Terjadi kesalahan dalam membuat pengguna");
//   }
// });

// edit user form
app.get("/users/edit/:uuid", async (req, res) => {
  try {
    const user_game = await User_Game.findOne({
      where: { uuid: req.params.uuid },
      include: [{ model: User_Game_Biodata, as: "biodata" }],
    });
    if (!user_game) {
      return res.status(404).send("User not found");
    }
    const user_game_biodata = user_game.biodata;
    res.render("edit", {
      user_game,
      user_game_biodata,
    });
  } catch (err) {
    console.error("Error fetching user data:", err);
    res.status(500).send("Internal Server Error");
  }
});

// update user
app.post("/users/edit/update/:uuid", async (req, res) => {
  const {
    username,
    email,
    password,
    role,
    firstname,
    lastname,
    gender,
    country,
    birthday,
  } = req.body;
  try {
    const [numRowsUpdated, [userGame]] = await User_Game.update(
      {
        username: username,
        email: email,
        password: password,
        role: role,
      },
      {
        where: { uuid: req.params.uuid },
        returning: true,
      }
    );
    if (numRowsUpdated !== 1) {
      throw new Error("User not found or not updated");
    }
    await User_Game_Biodata.update(
      {
        first_name: firstname,
        last_name: lastname,
        gender: gender,
        country: country,
        date_of_birth: birthday,
      },
      {
        where: { user_id: userGame.id },
      }
    );
    const user_game = await User_Game.findAll();
    const user_game_biodata = await User_Game_Biodata.findAll();
    const user_game_history = await User_Game_History.findAll();
    res.render("dashboard", {
      user_game,
      user_game_biodata,
      user_game_history,
    });
  } catch (err) {
    res.status(500).json("Can't update the user");
  }
});

// delete user
app.post("/users/delete/:uuid", async (req, res) => {
  try {
    await User_Game.destroy({
      where: { uuid: req.params.uuid },
    });
    const user_game = await User_Game.findAll();
    const user_game_biodata = await User_Game_Biodata.findAll();
    const user_game_history = await User_Game_History.findAll();
    res.render("dashboard", {
      user_game,
      user_game_biodata,
      user_game_history,
    });
  } catch (err) {
    console.error("Error deleting user:", err);
    res
      .status(500)
      .send(
        "An error occurred while deleting the user and associated biodata."
      );
  }
});

// user profile route
app.use(userProfile);

// error handler route
app.use(errorHandler);

// Port Listen
app.listen(port, async () => {
  console.log(`app listening to port ${port}`);
  await sequelize.authenticate();
  console.log("database connected bro!");
});




// CODE BELOW FOR TESTING PURPOSES ONLY!

// app.post("/test/users/create", async (req, res) => {
//   try {
//     const userGame = await User_Game.create({
//       username: req.body.username,
//       email: req.body.email,
//       password: req.body.password,
//       role: req.body.role,
//     });
//     await User_Game_Biodata.create({
//       first_name: req.body.firstname,
//       last_name: req.body.lastname,
//       gender: req.body.gender,
//       country: req.body.country,
//       user_id: userGame.id,
//       date_of_birth: req.body.birthday,
//     });
//     await User_Game_History.create({
//       win: req.body.win,
//       lose: req.body.lose,
//       total_match: req.body.totalmatch,
//       user_id: userGame.id,
//     });
//     res.json(userGame);
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Terjadi kesalahan dalam membuat pengguna");
//   }
// });

// app.get("/test/datas", async (req, res) => {
//   try {
//     const userGame = await User_Game.findAll({
//       include: [
//         { model: User_Game_Biodata, as: "biodata" },
//         { model: User_Game_History, as: "history" },
//       ],
//     });
//     res.json(userGame);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });

// app.post("/test/post", async (req, res) => {
//   try {
//     const userGame = await User_Game.create(req.body);
//     await User_Game_Biodata.create(req.body);
//     await User_Game_History.create(req.body);
//     res.json(userGame);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });

// app.get("/test", async (req, res) => {
//   try {
//     const userGame = await User_Game.findAll({
//       include: [{ model: User_Game_Biodata, as: "biodata" }],
//     });
//     res.json(userGame);
//   } catch (err) {
//     res.status(500).json(error);
//   }
// });